/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   data_holder.h
 * Author: root
 *
 * Created on December 11, 2016, 3:11 AM
 */

#ifndef DATA_HOLDER_H
#define DATA_HOLDER_H

using namespace std;

class data_holder {
public:

    data_holder(long key, char* data, int data_size, int version) : key(key), data(data), data_size(data_size), version(version) {
        
    }

    ~data_holder() {
        this->data_size = 0;
        free(data);
        this->data = nullptr;
    }

    long task_id = -1;
    long key = -1;
    char* data = nullptr;
    int version = -1;
    int data_size = 0;


};


#endif /* DATA_HOLDER_H */


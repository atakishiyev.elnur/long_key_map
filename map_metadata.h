/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   map_metadata.h
 * Author: root
 *
 * Created on December 11, 2016, 3:15 AM
 */

#ifndef MAP_METADATA_H
#define MAP_METADATA_H

#include <mutex>
#include <queue>
#include <unordered_map>
#include "Scheduler.h"
#include "data_holder.h"
using namespace std;

class map_metadata {
public:

    atomic_long size_in_bytes{0};

    map_metadata() : eviction_mutex(), eviction_con_var(), eviction_queue(), map(new unordered_map<long, data_holder*>()), scheduler(new Scheduler(1)) {

    }

    map_metadata(size_t size) : eviction_mutex(), eviction_con_var(), eviction_queue(), map(new unordered_map<long, data_holder*>(size)), scheduler(new Scheduler(1)) {

    }

    void reset(int size){
        map->clear();
        delete map;
        map = new unordered_map<long, data_holder*>(size);
        scheduler->reset();
    }
    
    long consumeEvictionsBlocking() {
        unique_lock<mutex> _lock(eviction_mutex);
        eviction_con_var.wait(_lock, [this] {
            return !eviction_queue.empty();
        });
        long key = eviction_queue.front();
        eviction_queue.pop();
        return key;
    }

    long consumeEvictionNonBlocking() {
        unique_lock<mutex> _lock(eviction_mutex);
        if (eviction_queue.empty()) {
            return -1;
        }
        long key = eviction_queue.front();
        eviction_queue.pop();
        return key;
    }

    void onExpired(long key) {
        std::unique_lock<mutex> _lock(eviction_mutex);
        eviction_queue.push(key);
        eviction_con_var.notify_one();
    }

    ~map_metadata() {
        map->clear();
        delete scheduler;
        delete map;
    }

    mutex eviction_mutex;
    condition_variable eviction_con_var;
    queue<long> eviction_queue;

    unordered_map<long, data_holder*>*map;
    Scheduler* scheduler;
};


#endif /* MAP_METADATA_H */


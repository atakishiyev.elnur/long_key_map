/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "linkedlogics_offheap_LongKeyConcurrentHashMap.h"
#include "map_metadata.h"
#include <mutex>
#include <string.h>
#include <atomic>

using namespace std;
mutex m;
//map_metadata** maps = new map_metadata*[1024];
unordered_map<int, map_metadata*> maps;
std::atomic_int idx{0};

/*
 * Class:     linkedlogics_offheap_LongKeyConcurrentHashMap
 * Method:    put
 * Signature: (IJ[BI)Z
 */
JNIEXPORT jboolean JNICALL Java_linkedlogics_offheap_LongKeyConcurrentHashMap_put__IJ_3BI
(JNIEnv * env, jobject, jint id, jlong key, jbyteArray data, jint version) {
    try {
        unique_lock<mutex> lock(m);
        jbyte* jb = (jbyte*) env->GetPrimitiveArrayCritical(data, NULL);
        map_metadata* _map_metadata = maps[id];
        if (_map_metadata) {
            int data_size = env->GetArrayLength(data);
            auto iter = _map_metadata->map->find(key);
            if (iter != _map_metadata->map->end()) {
                data_holder* dh = iter->second;
                if (dh->version <= version || version < 0) {
                    _map_metadata->scheduler->cancel(dh->task_id);
                    _map_metadata->size_in_bytes.operator-=(dh->data_size);
                    delete dh;


                    char* cb = (char*) malloc(data_size);
                    memmove(cb, jb, data_size);

                    _map_metadata->map->operator[](key) = new data_holder(key, cb, data_size, version);
                    _map_metadata->size_in_bytes.operator+=(data_size);

                    env->ReleasePrimitiveArrayCritical(data, jb, JNI_COMMIT);
                    return true;
                } else return false;
            } else {
                char* cb = (char*) malloc(data_size);
                memmove(cb, jb, data_size);

                _map_metadata->map->insert({key, new data_holder(key, cb, data_size, version)});
                _map_metadata->size_in_bytes.operator+=(data_size);
                env->ReleasePrimitiveArrayCritical(data, jb, JNI_COMMIT);
                return true;
            }
        }
        env->ReleasePrimitiveArrayCritical(data, jb, JNI_COMMIT);
        return false;
    } catch (exception) {
        return false;
    }
}

/*
 * Class:     linkedlogics_offheap_LongKeyConcurrentHashMap
 * Method:    put
 * Signature: (IJ[BJI)Z
 */
JNIEXPORT jboolean JNICALL Java_linkedlogics_offheap_LongKeyConcurrentHashMap_put__IJ_3BJI
(JNIEnv * env, jobject, jint id, jlong key, jbyteArray data, jlong expiry, jint version) {
    try {
        unique_lock<mutex> lock(m);
        map_metadata* _map_metadata = maps[id];
        if (_map_metadata) {
            int data_size = env->GetArrayLength(data);
            auto iter = _map_metadata->map->find(key);
            if (iter != _map_metadata->map->end()) {
                data_holder* dh = iter->second;
                if (dh->version <= version || version < 0) {
                    _map_metadata->scheduler->cancel(dh->task_id);
                    _map_metadata->size_in_bytes.operator-=(dh->data_size);
                    delete dh;

                    jbyte* jb = (jbyte*) env->GetPrimitiveArrayCritical(data, NULL);
                    char* cb = (char*) malloc(data_size);
                    memmove(cb, jb, data_size);

                    dh = new data_holder(key, cb, data_size, version);
                    dh->task_id = _map_metadata->scheduler->schedule(seconds(expiry), &map_metadata::onExpired, _map_metadata, key);
                    _map_metadata->map->operator[](key) = dh;
                    _map_metadata->size_in_bytes.operator+=(data_size);

                    env->ReleasePrimitiveArrayCritical(data, jb, JNI_COMMIT);
                    return true;
                } else return false;
            } else {
                jbyte* jb = (jbyte*) env->GetPrimitiveArrayCritical(data, NULL);
                char* cb = (char*) malloc(data_size);
                memmove(cb, jb, data_size);

                data_holder* dh = new data_holder(key, cb, data_size, version);
                dh->task_id = _map_metadata->scheduler->schedule(seconds(expiry), &map_metadata::onExpired, _map_metadata, key);

                _map_metadata->map->insert({key, dh});
                _map_metadata->size_in_bytes.operator+=(data_size);
                env->ReleasePrimitiveArrayCritical(data, jb, JNI_COMMIT);
                return true;
            }
        }
        return false;
    } catch (exception) {
        return false;
    }
}

/*
 * Class:     linkedlogics_offheap_LongKeyConcurrentHashMap
 * Method:    get
 * Signature: (IJ)[B
 */
JNIEXPORT jbyteArray JNICALL Java_linkedlogics_offheap_LongKeyConcurrentHashMap_get
(JNIEnv * env, jobject, jint id, jlong key) {
    unique_lock<mutex> lock(m);
    map_metadata* _map_metadata = maps[id];
    if (_map_metadata) {
        auto iter = _map_metadata->map->find(key);
        if (iter != _map_metadata->map->end()) {
            data_holder* dh = iter->second;
            if (dh->data_size > 0) {
                jbyteArray jbA = env->NewByteArray(dh->data_size);
                if (jbA) {
                    env->SetByteArrayRegion(jbA, 0, dh->data_size, (jbyte*) dh->data);
                    return jbA;
                } else return nullptr;
            } else return nullptr;
        } else return nullptr;
    } else return nullptr;
}

/*
 * Class:     linkedlogics_offheap_LongKeyConcurrentHashMap
 * Method:    remove
 * Signature: (IJ)V
 */
JNIEXPORT jboolean JNICALL Java_linkedlogics_offheap_LongKeyConcurrentHashMap_remove__IJ
(JNIEnv *, jobject, jint id, jlong key) {
    unique_lock<mutex> lock(m);
    map_metadata* _map_metadata = maps[id];
    if (_map_metadata) {
        auto iter = _map_metadata->map->find(key);
        if (iter != _map_metadata->map->end()) {
            _map_metadata->scheduler->cancel(iter->second->task_id);

            data_holder * dh = iter->second;
            _map_metadata->size_in_bytes.operator-=(dh->data_size);
            _map_metadata->map->erase(iter);
            delete dh;

            return true;
        } else return false;
    } else return false;

}

/*
 * Class:     linkedlogics_offheap_LongKeyConcurrentHashMap
 * Method:    remove
 * Signature: (IJI)V
 */
JNIEXPORT jboolean JNICALL Java_linkedlogics_offheap_LongKeyConcurrentHashMap_remove__IJI
(JNIEnv *, jobject, jint id, jlong key, jint version) {
    unique_lock<mutex> lock(m);
    map_metadata* _map_metadata = maps[id];
    if (_map_metadata) {
        auto iter = _map_metadata->map->find(key);
        if (iter != _map_metadata->map->end() && iter->second->version <= version) {

            data_holder *dh = iter->second;
            _map_metadata->size_in_bytes.operator-=(dh->data_size);
            _map_metadata->scheduler->cancel(iter->second->task_id);
            _map_metadata->map->erase(iter);
            delete dh;

            return true;
        } else return false;
    } else return false;
}

/*
 * Class:     linkedlogics_offheap_LongKeyConcurrentHashMap
 * Method:    suspendEviction
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_linkedlogics_offheap_LongKeyConcurrentHashMap_suspendEviction
(JNIEnv *, jobject, jint id) {
    unique_lock<mutex> lock(m);
    map_metadata* _map_metadata = maps[id];
    if (_map_metadata) {
        _map_metadata->scheduler->suspend();
    }
}

/*
 * Class:     linkedlogics_offheap_LongKeyConcurrentHashMap
 * Method:    resumeEviction
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_linkedlogics_offheap_LongKeyConcurrentHashMap_resumeEviction
(JNIEnv *, jobject, jint id) {
    unique_lock<mutex> lock(m);
    map_metadata* _map_metadata = maps[id];
    if (_map_metadata) {
        _map_metadata->scheduler->resume();
    }
}

/*
 * Class:     linkedlogics_offheap_LongKeyConcurrentHashMap
 * Method:    consumeEvictions
 * Signature: (I)J
 */
JNIEXPORT jlong JNICALL Java_linkedlogics_offheap_LongKeyConcurrentHashMap_consumeEvictions
(JNIEnv *, jobject, jint id) {
    unique_lock<mutex> lock(m);
    map_metadata* _map_metadata = maps[id];
    if (_map_metadata) {
        return _map_metadata->consumeEvictionsBlocking();
    }
    return -1;
}

/*
 * Class:     linkedlogics_offheap_LongKeyConcurrentHashMap
 * Method:    consumeEvictionsNonBlocking
 * Signature: (I)J
 */
JNIEXPORT jlong JNICALL Java_linkedlogics_offheap_LongKeyConcurrentHashMap_consumeEvictionsNonBlocking
(JNIEnv *, jobject, jint id) {
    unique_lock<mutex> lock(m);
    map_metadata* _map_metadata = maps[id];
    if (_map_metadata) {
        return _map_metadata->consumeEvictionsBlocking();
    }
    return -1;

}

/*
 * Class:     linkedlogics_offheap_LongKeyConcurrentHashMap
 * Method:    createMap
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_linkedlogics_offheap_LongKeyConcurrentHashMap_createMap__
(JNIEnv *, jobject) {
    unique_lock<mutex> lock(m);
    //    maps[idx] = new map_metadata();
    maps.insert(std::make_pair<int, map_metadata*>(idx.load(), new map_metadata()));
    return idx.fetch_add(1);

}

/*
 * Class:     linkedlogics_offheap_LongKeyConcurrentHashMap
 * Method:    createMap
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_linkedlogics_offheap_LongKeyConcurrentHashMap_createMap__I
(JNIEnv *, jobject, jint size) {
    unique_lock<mutex> lock(m);
    //    maps[idx] = new map_metadata(size);
    maps.insert(std::make_pair<int, map_metadata*>(idx.load(), new map_metadata(size)));
    return idx.fetch_add(1);

}

/*
 * Class:     linkedlogics_offheap_LongKeyConcurrentHashMap
 * Method:    size
 * Signature: (I)J
 */
JNIEXPORT jlong JNICALL Java_linkedlogics_offheap_LongKeyConcurrentHashMap_size
(JNIEnv *, jobject, jint id) {
    unique_lock<mutex> lock(m);
    map_metadata * _map_metadata = maps[id];
    if (_map_metadata) {
        return _map_metadata->map->size();
    }
    return -1;
}

JNIEXPORT jlong JNICALL Java_linkedlogics_offheap_LongKeyConcurrentHashMap_sizeInBytes
(JNIEnv *, jobject, jint id) {
    unique_lock<mutex> lock(m);
    map_metadata * _map_metadata = maps[id];
    if (_map_metadata) {
        return _map_metadata->size_in_bytes.load();
    }
    return -1;
}

/*
 * Class:     linkedlogics_offheap_LongKeyConcurrentHashMap
 * Method:    reset
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_linkedlogics_offheap_LongKeyConcurrentHashMap_reset
(JNIEnv *, jobject, jint id, jint size) {
    unique_lock<mutex> lock(m);
    map_metadata* _map_metadata = maps[id];
    if (_map_metadata) {
        if(size >0)
        _map_metadata->reset(size);
        else
            delete _map_metadata;
    }
}
